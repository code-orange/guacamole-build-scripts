#!/bin/bash
set -e
set +x

DIR="$( cd "$( dirname "$0" )" && pwd )"

. $DIR/config

wget "https://apache.org/dyn/closer.cgi?action=download&filename=guacamole/${version}/binary/guacamole-${version}.war" -O /var/lib/tomcat9/webapps/guacamole.war

wget "https://apache.org/dyn/closer.cgi?action=download&filename=guacamole/${version}/binary/guacamole-auth-ldap-${version}.tar.gz" -O /tmp/guacamole-auth-ldap.tar.gz

mkdir /tmp/guacamole-auth-ldap || true
tar -zxvf /tmp/guacamole-auth-ldap.tar.gz --strip 1 -C /tmp/guacamole-auth-ldap

mkdir -p /opt/guacamole || true
cp /tmp/guacamole-auth-ldap/guacamole-auth-ldap-*.jar /opt/guacamole/guacamole-auth-ldap.jar

mkdir -p /etc/guacamole || true
mkdir -p /etc/guacamole/extensions || true

chmod +x /etc/guacamole
chmod +x /etc/guacamole/extensions

rm -f /etc/guacamole/extensions/guacamole-auth-ldap.jar
ln -s /opt/guacamole/guacamole-auth-ldap.jar /etc/guacamole/extensions/guacamole-auth-ldap.jar
